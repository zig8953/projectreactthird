import React from 'react';
import { renderRoutes } from 'react-router-config';
import 'bootstrap/dist/css/bootstrap.min.css'

export const AuthLayout = props => {
    return (
        <div>
            <div class="d-flex justify-content-center">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Authorization</h4>
                </div>
            </div>
            <main>{renderRoutes(props.route.routes)}</main>
        </div>
        )
}