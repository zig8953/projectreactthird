import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Menu } from '../components/Menu';

export const MainLayout = props => {
    return (
        <div>
            <div class="d-flex justify-content-center">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Main</h4>
                </div>
            </div>
            <aside>
                <nav><Menu /></nav>
            </aside>
            <main>{renderRoutes(props.route.routes)}</main>
        </div>
    )
}