import React from 'react';
import { connect } from 'react-redux';
import { changeTitle } from '../../actions/title';
  
import 'bootstrap/dist/css/bootstrap.min.css'

export function Login(props) {
 
    return (
        <div>
            <Display/>
        </div>
    );
}

class DisplayComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            localTitle: ''
        };
    }

    onClickChangeTitle = (e) => {
        this.props.onChangeTitle(this.state.localTitle)
    }

    changeLocalTitle = (e) => {
        this.setState({ localTitle: e.target.value });
    }

    render() {
        return (
           
            <div class="d-flex justify-content-center">
                <div class="alert alert-success" role="alert">
                   

                    <h1>{this.props.title}</h1>
                    <div>
                        <label>Display Title:</label>
                        <br />
                        <input type="text" onChange={this.changeLocalTitle}></input>
                        <br />
                        <br />
                        <button type="submit" class="btn btn-primary" onClick={this.onClickChangeTitle} >Change title</button>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        title: state.title
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeTitle: title => {
            dispatch(changeTitle(title))
        }
    }
}

export const Display = connect(mapStateToProps, mapDispatchToProps)(DisplayComponent)