import React from 'react';
import { Link } from 'react-router-dom';

export const Menu = () => {
    return (
        <div class="d-flex justify-content-center">
            <ul>
                <li>
                    <Link to="/Home">Home</Link>
                </li>
                <li>
                    <Link to="login">Login</Link>
                </li>
                <li>
                    <Link to="register">Register</Link>
                </li>
            </ul>
        </div>
    )
}
