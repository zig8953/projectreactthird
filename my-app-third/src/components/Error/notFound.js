import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'

export function NotFound(props) {
    return (
        <div class="d-flex justify-content-center">
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Not Found 404</h4>
            </div>
        </div>

    );
}


