import { AuthLayout } from './layouts/auth';
import { MainLayout } from './layouts/main';

import { Login } from "./components/Login";
import { Register } from "./components/Register";
import { NotFound } from "./components/Error/notFound";
import { Home } from "./components/Home";

export const routes = [
    {
        path: "/login",
        component: AuthLayout,
        routes: [
            {
                component: Login
            }
        ]
    },
    {
        path: "*",
        component: MainLayout,
        routes: [
            {
                path: "/register",
                component: Register
            },
            {
                path: "/Home",
                component: Home
            },
            {
                path: "*",
                component: NotFound
            }

        ]
    }  
];